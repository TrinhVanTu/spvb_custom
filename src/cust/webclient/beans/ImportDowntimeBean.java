package cust.webclient.beans;

import java.io.BufferedReader;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStreamReader;
import java.nio.ByteBuffer;
import java.nio.charset.CharacterCodingException;
import java.nio.charset.Charset;
import java.rmi.RemoteException;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import psdi.security.ConnectionKey;
import psdi.server.MXServer;
import psdi.util.MXApplicationException;
import psdi.util.MXException;
import psdi.webclient.system.beans.DataBean;
import psdi.webclient.system.controller.UploadFile;
import psdi.webclient.system.controller.WebClientEvent;

public class ImportDowntimeBean extends DataBean {
    public static final String UTF8_BOM = "\ufeff";

    public ImportDowntimeBean() {
    }

    protected void initialize() throws MXException, RemoteException {
    }

    public int execute() throws MXException, RemoteException {
        if (this.clientSession.runLongOp(this, "execute")) {
            WebClientEvent currentEvent = this.clientSession.getCurrentEvent();
            UploadFile file = (UploadFile)this.app.get("importfile");

            try {
                HashMap<String, List<List<String>>> hashMap = this.readFlatFile(file);
                List<String> listHeaders = (List)((List)hashMap.get("header")).get(0);
                List<List<String>> listValues = hashMap.get("rows");
                int numRows = listValues.size();
                this.validateNumberOfRows(numRows);
                this.validateDateRangesOverlapRecordsImport(listHeaders, listValues);
                this.validateDuplicationRecords(listHeaders, listValues);
                if (listValues.size() == 0) {
                    this.clientSession.showMessageBox(currentEvent, "importdowntime", "importfailed", new String[]{"Template upload không hợp lệ, vui lòng kiểm tra lại định dạng."});
                }

                int i;
                for(i = 0; i < listValues.size(); ++i) {
                    HashMap<String, String> hmap = this.pustIntoHmap(listHeaders, listValues.get(i));
                    this.validateRequiredFields(hmap.get("START DATE"), hmap.get("END DATE"), hmap.get("FILLER"), hmap.get("FILLER START"), hmap.get("FILLER END"), hmap.get("C"), hmap.get("I"), hmap.get("L"), hmap.get("T"), i + 2);
                    if (!hmap.get("START DATE").isEmpty() && !hmap.get("END DATE").isEmpty()) {
                        this.isThisDateValid(hmap.get("START DATE"), "mm/dd/yyyy HH:mm:ss", i + 2);
                        this.isThisDateValid(hmap.get("END DATE"), "mm/dd/yyyy HH:mm:ss", i + 2);
                        this.validateEnddateAfterStartDate(new Date(hmap.get("START DATE")), new Date(hmap.get("END DATE")), i + 2);
                    }

                    if (!hmap.get("FILLER START").isEmpty() && !hmap.get("FILLER END").isEmpty()) {
                        this.isThisDateValid(hmap.get("FILLER START"), "mm/dd/yyyy HH:mm:ss", i + 2);
                        this.isThisDateValid(hmap.get("FILLER END"), "mm/dd/yyyy HH:mm:ss", i + 2);
                        this.validateEnddateAfterStartDate(new Date(hmap.get("FILLER START")), new Date(hmap.get("FILLER END")), i + 2);
                    }

                    this.validateDateRangesOverlap(hmap.get("ASSETNUM"), hmap.get("SITEID"), hmap.get("START DATE"), hmap.get("END DATE"), i + 2);
                    if (!hmap.get("FILLER").equals("")) {
                        this.validateDateRangesOverlap(hmap.get("FILLER"), hmap.get("SITEID"), hmap.get("FILLER START"), hmap.get("FILLER END"), i + 2);
                    }

                    this.validateAsset(hmap.get("ASSETNUM"), hmap.get("SITEID"), i + 2);
                    if (!hmap.get("FILLER").equals("")) {
                        this.validateAsset(hmap.get("FILLER"), hmap.get("SITEID"), i + 2);
                        if (hmap.get("FILLER").equals(hmap.get("ASSETNUM"))) {
                            this.clientSession.addWarning(new MXApplicationException("importdowntime", "importError", new String[]{"Mã Filler không được trùng mã asset tại dòng: " + (i + 2)}));
                        }
                    }

                    this.validateDowntimeCode(hmap.get("DOWNTIME CODE"), i + 2);
                }

                if (!this.clientSession.hasWarnings()) {
                    int checkComplete = 0;

                    for(i = 0; i < listValues.size(); ++i) {
                        HashMap<String, String> hmap = this.pustIntoHmap(listHeaders, listValues.get(i));
                        this.insertIntoASSETSTATUS(hmap);
                        ++checkComplete;
                    }

                    if (checkComplete == listValues.size()) {
                        if (checkComplete != 0) {
                            this.clientSession.showMessageBox(currentEvent, "importdowntime", "importsuccess", new String[]{"Import Successful"});
                        }

                        this.fireDataChangedEvent();
                    }
                }
            } catch (Exception var10) {
                this.clientSession.showMessageBox(currentEvent, "importdowntime", "importfailed", new String[]{"Import Failed"});
                return 1;
            }
        }

        return 1;
    }

    public HashMap<String, String> pustIntoHmap(List<String> header, List<String> values) {
        HashMap<String, String> hmap = new HashMap();
        hmap.clear();
        int size = header.size();

        for(int i = 0; i < size; ++i) {
            try {
                hmap.put(header.get(i), values.get(i));
            } catch (Exception var7) {
                hmap.put(header.get(i), "");
            }
        }

        return hmap;
    }

    private static String getFormatDateTime(String string) {
        Date date = new Date(string);
        SimpleDateFormat formatter = new SimpleDateFormat("yyyy/MM/dd HH:mm:ss");
        String strDate = formatter.format(date);
        return strDate;
    }

    public int insertIntoASSETSTATUS(HashMap<String, String> hmap) throws MXException, RemoteException, SQLException {
        MXServer mxServer = MXServer.getMXServer();
        ConnectionKey conKey = mxServer.getSystemUserInfo().getConnectionKey();
        Connection con = mxServer.getDBManager().getConnection(conKey);

        try {
            PreparedStatement stmnt = null;
            String sqlIdentifier;
            if (hmap.get("FILLER") == "") {
                sqlIdentifier = "INSERT INTO ASSETSTATUS(ASSETNUM, ISRUNNING, CHANGEDATE, CHANGEBY, DOWNTIME, CODE, OPERATIONAL ,LOCATION ,SITEID ,ORGID, ASSETSTATUSID, SPVB_C, SPVB_I, SPVB_L, SPVB_T, SPVB_RELATEDDOWNTIME, REMARKS, SPVB_ISSUE, SPVB_CA, SPVB_PA) VALUES (?, ?, TO_DATE(?, 'yyyy/mm/dd hh24:mi:ss'), ? ,?, ? ,? ,? ,? ,?, ASSETSTATUSSEQ.NEXTVAL, ?, ?, ?, ?, ?, ?, ?, ?, ?)";
                stmnt = con.prepareStatement(sqlIdentifier);
                String[] arr = this.getAssetInformation(hmap.get("ASSETNUM"), hmap.get("SITEID"));
                stmnt.setString(1, hmap.get("ASSETNUM"));
                stmnt.setInt(2, 0);
                stmnt.setString(3, getFormatDateTime(hmap.get("START DATE")));
                stmnt.setString(4, this.getCurrentPersonId());
                stmnt.setFloat(5, 0.0F);
                stmnt.setString(6, hmap.get("DOWNTIME CODE"));
                stmnt.setInt(7, 1);
                stmnt.setString(8, arr[0]);
                stmnt.setString(9, arr[1]);
                stmnt.setString(10, arr[2]);
                stmnt.setString(11, hmap.get("C"));
                stmnt.setString(12, hmap.get("I"));
                stmnt.setString(13, hmap.get("L"));
                stmnt.setString(14, hmap.get("T"));
                stmnt.setString(15, "0");
                stmnt.setString(16, hmap.get("REMARKS"));
                stmnt.setString(17, hmap.get("ISSUE"));
                stmnt.setString(18, hmap.get("CORRECTIVE ACTION"));
                stmnt.setString(19, hmap.get("PREVENTIVE ACTION"));
                stmnt.executeUpdate();
                String query1 = "INSERT INTO ASSETSTATUS(ASSETNUM, ISRUNNING, CHANGEDATE, CHANGEBY, DOWNTIME, CODE, OPERATIONAL ,LOCATION ,SITEID ,ORGID, ASSETSTATUSID, SPVB_C, SPVB_I, SPVB_L, SPVB_T, SPVB_RELATEDDOWNTIME, REMARKS, SPVB_ISSUE, SPVB_CA, SPVB_PA) VALUES (?, ?, TO_DATE(?, 'yyyy/mm/dd hh24:mi:ss'), ? ,?, ? ,? ,? ,? ,?, ASSETSTATUSSEQ.NEXTVAL, ?, ?, ?, ?, ?, ?, ?, ?, ?)";
                stmnt = con.prepareStatement(query1);
                stmnt.setString(1, hmap.get("ASSETNUM"));
                stmnt.setInt(2, 1);
                stmnt.setString(3, getFormatDateTime(hmap.get("END DATE")));
                stmnt.setString(4, this.getCurrentPersonId());
                float diff = this.getDiffTwoTime(new Date(hmap.get("START DATE")), new Date(hmap.get("END DATE")));
                stmnt.setFloat(5, diff);
                stmnt.setString(6, hmap.get("DOWNTIME CODE"));
                stmnt.setInt(7, 1);
                stmnt.setString(8, arr[0]);
                stmnt.setString(9, arr[1]);
                stmnt.setString(10, arr[2]);
                stmnt.setString(11, hmap.get("C"));
                stmnt.setString(12, hmap.get("I"));
                stmnt.setString(13, hmap.get("L"));
                stmnt.setString(14, hmap.get("T"));
                stmnt.setString(15, "");
                stmnt.setString(16, "");
                stmnt.setString(17, hmap.get("ISSUE"));
                stmnt.setString(18, hmap.get("CORRECTIVE ACTION"));
                stmnt.setString(19, hmap.get("PREVENTIVE ACTION"));
                stmnt.executeUpdate();
            }

            if (hmap.get("FILLER") != "") {
                sqlIdentifier = "select ASSETSTATUSSEQ.NEXTVAL from dual";
                PreparedStatement pst = con.prepareStatement(sqlIdentifier);
                long myId = 0L;
                synchronized(this) {
                    ResultSet rs = pst.executeQuery();
                    if (rs.next()) {
                        myId = rs.getLong(1);
                    }
                }

                String query4 = "INSERT INTO ASSETSTATUS(ASSETNUM, ISRUNNING, CHANGEDATE, CHANGEBY, DOWNTIME, CODE, OPERATIONAL ,LOCATION ,SITEID ,ORGID, ASSETSTATUSID, SPVB_C, SPVB_I, SPVB_L, SPVB_T, SPVB_RELATEDDOWNTIME, REMARKS, SPVB_ISSUE, SPVB_CA, SPVB_PA) VALUES (?, ?, TO_DATE(?, 'yyyy/mm/dd hh24:mi:ss'), ? ,?, ? ,? ,? ,? ,?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)";
                stmnt = con.prepareStatement(query4);
                String[] arr3 = this.getAssetInformation(hmap.get("ASSETNUM"), hmap.get("SITEID"));
                stmnt.setString(1, hmap.get("ASSETNUM"));
                stmnt.setInt(2, 0);
                stmnt.setString(3, getFormatDateTime(hmap.get("START DATE")));
                stmnt.setString(4, this.getCurrentPersonId());
                stmnt.setFloat(5, 0.0F);
                stmnt.setString(6, hmap.get("DOWNTIME CODE"));
                stmnt.setInt(7, 1);
                stmnt.setString(8, arr3[0]);
                stmnt.setString(9, arr3[1]);
                stmnt.setString(10, arr3[2]);
                stmnt.setLong(11, myId);
                stmnt.setString(12, hmap.get("C"));
                stmnt.setString(13, hmap.get("I"));
                stmnt.setString(14, hmap.get("L"));
                stmnt.setString(15, hmap.get("T"));
                stmnt.setString(16, "");
                stmnt.setString(17, hmap.get("REMARKS"));
                stmnt.setString(18, hmap.get("ISSUE"));
                stmnt.setString(19, hmap.get("CORRECTIVE ACTION"));
                stmnt.setString(20, hmap.get("PREVENTIVE ACTION"));
                stmnt.executeUpdate();
                String query3 = "INSERT INTO ASSETSTATUS(ASSETNUM, ISRUNNING, CHANGEDATE, CHANGEBY, DOWNTIME, CODE, OPERATIONAL ,LOCATION ,SITEID ,ORGID, ASSETSTATUSID, SPVB_C, SPVB_I, SPVB_L, SPVB_T, SPVB_RELATEDDOWNTIME, REMARKS, SPVB_ISSUE, SPVB_CA, SPVB_PA) VALUES (?, ?, TO_DATE(?, 'yyyy/mm/dd hh24:mi:ss'), ? ,?, ? ,? ,? ,? ,?, ASSETSTATUSSEQ.NEXTVAL, ?, ?, ?, ?, ?, ?, ?, ?, ?)";
                stmnt = con.prepareStatement(query3);
                stmnt.setString(1, hmap.get("ASSETNUM"));
                stmnt.setInt(2, 1);
                stmnt.setString(3, getFormatDateTime(hmap.get("END DATE")));
                stmnt.setString(4, this.getCurrentPersonId());
                float diff3 = this.getDiffTwoTime(new Date(hmap.get("START DATE")), new Date(hmap.get("END DATE")));
                stmnt.setFloat(5, diff3);
                stmnt.setString(6, hmap.get("DOWNTIME CODE"));
                stmnt.setInt(7, 1);
                stmnt.setString(8, arr3[0]);
                stmnt.setString(9, arr3[1]);
                stmnt.setString(10, arr3[2]);
                stmnt.setString(11, hmap.get("C"));
                stmnt.setString(12, hmap.get("I"));
                stmnt.setString(13, hmap.get("L"));
                stmnt.setString(14, hmap.get("T"));
                stmnt.setString(15, "");
                stmnt.setString(16, "");
                stmnt.setString(17, hmap.get("ISSUE"));
                stmnt.setString(18, hmap.get("CORRECTIVE ACTION"));
                stmnt.setString(19, hmap.get("PREVENTIVE ACTION"));
                stmnt.executeUpdate();
                String query2 = "INSERT INTO ASSETSTATUS(ASSETNUM, ISRUNNING, CHANGEDATE, CHANGEBY, DOWNTIME, CODE, OPERATIONAL ,LOCATION ,SITEID ,ORGID, ASSETSTATUSID, SPVB_C, SPVB_I, SPVB_L, SPVB_T, SPVB_RELATEDDOWNTIME, REMARKS, SPVB_ISSUE, SPVB_CA, SPVB_PA) VALUES (?, ?, TO_DATE(?, 'yyyy/mm/dd hh24:mi:ss'), ? ,?, ? ,? ,? ,? ,?, ASSETSTATUSSEQ.NEXTVAL, ?, ?, ?, ?, ?, ?, ?, ?, ?)";
                stmnt = con.prepareStatement(query2);
                String[] arr = this.getAssetInformation(hmap.get("FILLER"), hmap.get("SITEID"));
                stmnt.setString(1, hmap.get("FILLER"));
                stmnt.setInt(2, 0);
                stmnt.setString(3, getFormatDateTime(hmap.get("FILLER START")));
                stmnt.setString(4, this.getCurrentPersonId());
                stmnt.setFloat(5, 0.0F);
                stmnt.setString(6, hmap.get("DOWNTIME CODE"));
                stmnt.setInt(7, 1);
                stmnt.setString(8, arr[0]);
                stmnt.setString(9, arr[1]);
                stmnt.setString(10, arr[2]);
                stmnt.setString(11, hmap.get("C"));
                stmnt.setString(12, hmap.get("I"));
                stmnt.setString(13, hmap.get("L"));
                stmnt.setString(14, hmap.get("T"));
                stmnt.setLong(15, myId);
                stmnt.setString(16, hmap.get("REMARKS"));
                stmnt.setString(17, hmap.get("ISSUE"));
                stmnt.setString(18, hmap.get("CORRECTIVE ACTION"));
                stmnt.setString(19, hmap.get("PREVENTIVE ACTION"));
                stmnt.executeUpdate();
                String query1 = "INSERT INTO ASSETSTATUS(ASSETNUM, ISRUNNING, CHANGEDATE, CHANGEBY, DOWNTIME, CODE, OPERATIONAL ,LOCATION ,SITEID ,ORGID, ASSETSTATUSID, SPVB_C, SPVB_I, SPVB_L, SPVB_T, SPVB_RELATEDDOWNTIME, REMARKS, SPVB_ISSUE, SPVB_CA, SPVB_PA) VALUES (?, ?, TO_DATE(?, 'yyyy/mm/dd hh24:mi:ss'), ? ,?, ? ,? ,? ,? ,?, ASSETSTATUSSEQ.NEXTVAL, ?, ?, ?, ?, ?, ?, ?, ?, ?)";
                stmnt = con.prepareStatement(query1);
                stmnt.setString(1, hmap.get("FILLER"));
                stmnt.setInt(2, 1);
                stmnt.setString(3, getFormatDateTime(hmap.get("FILLER END")));
                stmnt.setString(4, this.getCurrentPersonId());
                float diff = this.getDiffTwoTime(new Date(hmap.get("FILLER START")), new Date(hmap.get("FILLER END")));
                stmnt.setFloat(5, diff);
                stmnt.setString(6, hmap.get("DOWNTIME CODE"));
                stmnt.setInt(7, 1);
                stmnt.setString(8, arr[0]);
                stmnt.setString(9, arr[1]);
                stmnt.setString(10, arr[2]);
                stmnt.setString(11, hmap.get("C"));
                stmnt.setString(12, hmap.get("I"));
                stmnt.setString(13, hmap.get("L"));
                stmnt.setString(14, hmap.get("T"));
                stmnt.setString(15, "");
                stmnt.setString(16, "");
                stmnt.setString(17, hmap.get("ISSUE"));
                stmnt.setString(18, hmap.get("CORRECTIVE ACTION"));
                stmnt.setString(19, hmap.get("PREVENTIVE ACTION"));
                stmnt.executeUpdate();
            }
        } catch (SQLException var24) {
            con.rollback();
            throw new MXApplicationException("SQLException caught", var24.getMessage());
        } finally {
            if (con != null) {
                mxServer.getDBManager().freeConnection(conKey);
            }

        }

        return 1;
    }

    public HashMap<String, List<List<String>>> readFlatFile(UploadFile fileupload) throws MXException, IOException {
        HashMap<String, List<List<String>>> hashMap = new HashMap();
        List<String> listHeaders = new ArrayList();
        List<List<String>> listValues = new ArrayList();
        String line = "";
        String cvsSplitBy = "\\t";
        BufferedReader br = null;
        ByteArrayOutputStream fileOutputStream = fileupload.getFileOutputStream();
        byte[] bytesarr = fileOutputStream.toByteArray();
        if (!isValidUTF8(bytesarr)) {
            this.clientSession.addWarning(new MXApplicationException("importdowntime", "importError", new String[]{"Template không thuộc định dạng UTF-8 encoding (.csv, .txt)"}));
        }

        InputStreamReader reader = new InputStreamReader(new ByteArrayInputStream(bytesarr), "UTF-16");

        try {
            br = new BufferedReader(reader);
            int i = 0;

            while(true) {
                while((line = br.readLine()) != null) {
                    ++i;
                    line = removeUTF8BOM(line);
                    String[] arrStr = line.split(cvsSplitBy);
                    int i$;
                    if (i == 1) {
                        String[] arr$ = arrStr;
                        int len$ = arrStr.length;

                        for(i$ = 0; i$ < len$; ++i$) {
                            String str = arr$[i$];
                            listHeaders.add(str != null && !str.trim().isEmpty() ? str : "");
                        }
                    } else {
                        List<String> rowValues = new ArrayList();
                        String[] arr$ = arrStr;
                        i$ = arrStr.length;

                        for(int d = 0; d < i$; ++d) {
                            String str = arr$[d];
                            rowValues.add(str != null && !str.trim().isEmpty() ? str : "");
                        }

                        listValues.add(rowValues);
                    }
                }

                List<List<String>> list = new ArrayList();
                list.add(listHeaders);
                hashMap.put("header", list);
                hashMap.put("rows", listValues);
                break;
            }
        } catch (FileNotFoundException var28) {
            var28.printStackTrace();
        } catch (IOException var29) {
            var29.printStackTrace();
        } finally {
            if (br != null) {
                try {
                    br.close();
                } catch (IOException var27) {
                    var27.printStackTrace();
                }
            }

        }

        return hashMap;
    }

    public int validateAsset(String assetnum, String siteid, int row) throws MXException, RemoteException, SQLException {
        MXServer mxServer = MXServer.getMXServer();
        ConnectionKey conKey = mxServer.getSystemUserInfo().getConnectionKey();
        Connection con = mxServer.getDBManager().getConnection(conKey);

        try {
            PreparedStatement stmnt = null;

            try {
                String query = "SELECT ASSETNUM FROM ASSET WHERE SITEID = '" + siteid + "' AND ASSETNUM = '" + assetnum + "'";
                stmnt = con.prepareStatement(query);
            } catch (SQLException var13) {
                System.out.println(var13);
            }

            if (stmnt.execute()) {
                ResultSet results = stmnt.getResultSet();
                if (!results.next()) {
                    this.clientSession.addWarning(new MXApplicationException("importdowntime", "importError", new String[]{"Asset " + assetnum + " không tồn tại ở dòng : " + row}));
                }
            }
        } catch (SQLException var14) {
            throw new MXApplicationException("SQLException caught", var14.getMessage());
        } finally {
            if (con != null) {
                mxServer.getDBManager().freeConnection(conKey);
            }

        }

        return 1;
    }

    public int validateDowntimeCode(String downtimeCode, int row) throws MXException, RemoteException, SQLException {
        MXServer mxServer = MXServer.getMXServer();
        ConnectionKey conKey = mxServer.getSystemUserInfo().getConnectionKey();
        Connection con = mxServer.getDBManager().getConnection(conKey);

        try {
            PreparedStatement stmnt = null;

            try {
                String query = "SELECT VALUE FROM ALNDOMAIN WHERE DOMAINID = 'DOWNCODE' AND VALUE = '" + downtimeCode + "'";
                stmnt = con.prepareStatement(query);
            } catch (SQLException var12) {
                System.out.println(var12);
            }

            if (stmnt.execute()) {
                ResultSet results = stmnt.getResultSet();
                if (!results.next()) {
                    this.clientSession.addWarning(new MXApplicationException("importdowntime", "importError", new String[]{"Downtime Code " + downtimeCode + " không tồn tại ở dòng : " + row}));
                }
            }
        } catch (SQLException var13) {
            throw new MXApplicationException("SQLException caught", var13.getMessage());
        } finally {
            if (con != null) {
                mxServer.getDBManager().freeConnection(conKey);
            }

        }

        return 1;
    }

    public boolean isThisDateValid(String dateToValidate, String dateFromat, int row) {
        if (dateToValidate == null) {
            return false;
        } else {
            SimpleDateFormat sdf = new SimpleDateFormat(dateFromat);
            sdf.setLenient(false);

            try {
                Date date = sdf.parse(dateToValidate);
                System.out.println(date);
                return true;
            } catch (ParseException var6) {
                this.clientSession.addWarning(new MXApplicationException("importdowntime", "importError", new String[]{"Ngày không hợp lệ " + dateToValidate + " ở dòng: " + row}));
                return false;
            }
        }
    }

    public void validateNumberOfRows(int num) {
        if (num > 200) {
            this.clientSession.addWarning(new MXApplicationException("importdowntime", "importError", new String[]{"Số lượng dòng import không được vượt quá 200"}));
        }

    }

    public boolean validateEnddateAfterStartDate(Date startDate, Date endDate, int row) {
        boolean bl = startDate.before(endDate);
        if (!bl) {
            this.clientSession.addWarning(new MXApplicationException("importdowntime", "importError", new String[]{"End Date phải lớn hơn Start Date ở dòng " + row}));
        }

        return bl;
    }

    public String[] getAssetInformation(String assetnum, String site) throws MXException, RemoteException, SQLException {
        String[] arr = new String[3];
        MXServer mxServer = MXServer.getMXServer();
        ConnectionKey conKey = mxServer.getSystemUserInfo().getConnectionKey();
        Connection con = mxServer.getDBManager().getConnection(conKey);

        try {
            PreparedStatement stmnt = null;

            try {
                String query = "SELECT LOCATION, SITEID, ORGID FROM ASSET WHERE ASSETNUM = '" + assetnum + "' AND SITEID = '" + site + "'";
                stmnt = con.prepareStatement(query);
            } catch (SQLException var15) {
                System.out.println(var15);
            }

            if (stmnt.execute()) {
                ResultSet results = stmnt.getResultSet();
                if (!results.next()) {
                    this.clientSession.addWarning(new MXApplicationException("importdowntime", "importError", new String[]{" Asset : " + assetnum + " không thuộc Site "}));
                } else {
                    String location = results.getString(1) != null ? results.getString(1) : "";
                    String siteid = results.getString(2) != null ? results.getString(2) : "";
                    String orgid = results.getString(3) != null ? results.getString(3) : "";
                    arr[0] = location;
                    arr[1] = siteid;
                    arr[2] = orgid;
                }
            }
        } catch (SQLException var16) {
            throw new MXApplicationException("SQLException caught", var16.getMessage());
        } finally {
            if (con != null) {
                mxServer.getDBManager().freeConnection(conKey);
            }

        }

        return arr;
    }

    public void validateDuplicationRecords(List<String> headers, List<List<String>> lsRows) {
        int indexOfAssetNum = 0;
        int indexOfStartDate = 0;

        int j;
        for(j = 0; j < headers.size(); ++j) {
            if (headers.get(j).equals("ASSETNUM")) {
                indexOfAssetNum = j;
            }

            if (headers.get(j).equals("START DATE")) {
                indexOfStartDate = j;
            }
        }

        for(j = 0; j < lsRows.size(); ++j) {
            for(int k = j + 1; k < lsRows.size(); ++k) {
                String check11;
                try {
                    check11 = (String)((List)lsRows.get(j)).get(indexOfAssetNum);
                } catch (Exception var15) {
                    check11 = "";
                }

                String check12;
                try {
                    check12 = (String)((List)lsRows.get(k)).get(indexOfAssetNum);
                } catch (Exception var14) {
                    check12 = "";
                }

                String check21;
                try {
                    check21 = (String)((List)lsRows.get(j)).get(indexOfStartDate);
                } catch (Exception var13) {
                    check21 = "";
                }

                String check22;
                try {
                    check22 = (String)((List)lsRows.get(k)).get(indexOfStartDate);
                } catch (Exception var12) {
                    check22 = "";
                }

                if (check11.equals(check12) && check21.equals(check22)) {
                    this.clientSession.addWarning(new MXApplicationException("importdowntime", "importError", new String[]{"Dòng  " + (j + 2) + " trùng lặp với dòng " + (k + 2)}));
                }
            }
        }

    }

    /*public void validateDateRangesOverlap(String assetnum, String siteid, String startDate, String endDate, int row) throws RemoteException, MXException {
        String[] arr = new String[3];
        MXServer mxServer = MXServer.getMXServer();
        ConnectionKey conKey = mxServer.getSystemUserInfo().getConnectionKey();
        Connection con = mxServer.getDBManager().getConnection(conKey);

        try {
            PreparedStatement stmnt = null;

            try {
                String sql = "SELECT  ASSETNUM FROM ASSETSTATUS WHERE assetnum = '" + assetnum + "' and siteid = '" + siteid + "' and changedate between TO_DATE(:1, 'MM/DD/YYYY HH24:MI') and TO_DATE(:2, 'MM/DD/YYYY HH24:MI')";
                stmnt = con.prepareStatement(sql);
                stmnt.setString(1, startDate);
                stmnt.setString(2, endDate);
            } catch (SQLException var16) {
                System.out.println(var16);
            }

            if (stmnt.execute()) {
                ResultSet results = stmnt.getResultSet();
                if (results.next()) {
                    this.clientSession.addWarning(new MXApplicationException("importdowntime", "importError", new String[]{"Asset " + assetnum + " ở dòng " + row + " đã tồn tại thời gian downtime trong hệ thống "}));
                }
            }
        } catch (SQLException var17) {
            throw new MXApplicationException("SQLException caught", var17.getMessage());
        } finally {
            if (con != null) {
                mxServer.getDBManager().freeConnection(conKey);
            }

        }

    }*/

    public void validateDateRangesOverlap(String assetnum, String siteid, String startDate, String endDate, int row) throws RemoteException, MXException {
        String[] arr = new String[3];
        MXServer mxServer = MXServer.getMXServer();
        ConnectionKey conKey = mxServer.getSystemUserInfo().getConnectionKey();
        Connection con = mxServer.getDBManager().getConnection(conKey);

        try {
            PreparedStatement stmnt = null;

            try {
                String sql = "SELECT to_char(CHANGEDATE, 'MM/DD/YYYY HH24:MI:SS'), DOWNTIME FROM ASSETSTATUS WHERE assetnum = '" + assetnum + "' and siteid = '" + siteid + "' and DOWNTIME > 0";
                stmnt = con.prepareStatement(sql);
            } catch (SQLException var16) {
                System.out.println(var16);
            }

            if (stmnt.execute()) {
                ResultSet results = stmnt.getResultSet();
                if (results.next()) {
                    //Determining if Two Date Ranges Overlap
                    String changeDate = results.getString(1);
                    float downtime = results.getFloat(2);
                    //changeDate - downtime
                    if(twoDateRangesOverlap(startDate, endDate, getStartDateFromChangeDate(changeDate, downtime), changeDate)) {
                        this.clientSession.addWarning(new MXApplicationException("importdowntime", "importError", new String[]{"Asset " + assetnum + " ở dòng " + row + " đã tồn tại thời gian downtime trong hệ thống "}));
                    }
                }
            }
        } catch (SQLException var17) {
            throw new MXApplicationException("SQLException caught", var17.getMessage());
        } finally {
            if (con != null) {
                mxServer.getDBManager().freeConnection(conKey);
            }

        }

    }

    //changeDate - downtime
    public String getStartDateFromChangeDate(String changeDateStr, float downtime) throws MXApplicationException {
        try {
            Date changeDate = new SimpleDateFormat("mm/dd/yyyy HH:mm:ss").parse(changeDateStr);
            Date date = new Date((long) (changeDate.getTime() - downtime * 3600000.0F));

            DateFormat dateFormat = new SimpleDateFormat("mm/dd/yyyy HH:mm:ss");
            String strDate = dateFormat.format(date);

            return strDate;
        } catch (Exception ex) {
            throw new MXApplicationException("Exception", ex.getMessage());
        }
    }

    public boolean twoDateRangesOverlap(String start1, String end1, String start2, String end2) throws MXApplicationException {
        try {
            Date startDate1 = new SimpleDateFormat("mm/dd/yyyy HH:mm:ss").parse(start1);
            Date endDate1 = new SimpleDateFormat("mm/dd/yyyy HH:mm:ss").parse(end1);
            Date startDate2 = new SimpleDateFormat("mm/dd/yyyy HH:mm:ss").parse(start2);
            Date endDate2 = new SimpleDateFormat("mm/dd/yyyy HH:mm:ss").parse(end2);
            //(startDate1 <= endDate2) and (startDate2 <= endDate1)
            if(startDate1.compareTo(endDate2) <= 0 && startDate2.compareTo(endDate1) <= 0) {
                return true;
            }
            return false;
        } catch (Exception ex) {
            throw new MXApplicationException("Exception", ex.getMessage());
        }
    }

    public String getCurrentPersonId() throws RemoteException, MXException {
        return this.clientSession.getUserInfo().getUserName();
    }

    public float getDiffTwoTime(Date startDate, Date endDate) {
        float diff = (float)(endDate.getTime() - startDate.getTime());
        float diffHours = diff / 3600000.0F;
        return diffHours;
    }

    public static boolean isValidUTF8(byte[] bytes) {
        try {
            Charset.availableCharsets().get("UTF-16").newDecoder().decode(ByteBuffer.wrap(bytes));
            return true;
        } catch (CharacterCodingException var2) {
            return false;
        }
    }

    public void validateDateRangesOverlapRecordsImport(List<String> headers, List<List<String>> lsRows) {
        int indexOfAssetNum = 0;
        int fill = 0;
        int indexOfStartDate = 0;
        int fillstart = 0;
        int indexOfEndDate = 0;
        int fillend = 0;

        int j;
        for(j = 0; j < headers.size(); ++j) {
            if (headers.get(j).equals("ASSETNUM")) {
                indexOfAssetNum = j;
            }

            if (headers.get(j).equals("FILLER")) {
                fill = j;
            }

            if (headers.get(j).equals("START DATE")) {
                indexOfStartDate = j;
            }

            if (headers.get(j).equals("FILLER START")) {
                fillstart = j;
            }

            if (headers.get(j).equals("END DATE")) {
                indexOfEndDate = j;
            }

            if (headers.get(j).equals("FILLER END")) {
                fillend = j;
            }
        }

        for(j = 0; j < lsRows.size(); ++j) {
            int k;
            String check11;
            String checkfill1;
            String check12;
            String checkfill2;
            Date StartDate1;
            Date StartFill1;
            Date EndDate1;
            Date EndFill1;
            Date StartDate2;
            Date StartFill2;
            Date EndDate2;
            Date EndFill2;
            for(k = 0; k < lsRows.size(); ++k) {
                //Lấy thông tin ASSETNUM at index j
                try {
                    check11 = (String)((List)lsRows.get(j)).get(indexOfAssetNum);
                } catch (Exception var35) {
                    check11 = "";
                }
                //Lấy thông tin FILLER at index k
                try {
                    checkfill2 = (String)((List)lsRows.get(k)).get(fill);
                } catch (Exception var32) {
                    checkfill2 = "";
                }
                //Lấy thông tin START DATE at index j
                try {
                    StartDate1 = new Date((String)((List)lsRows.get(j)).get(indexOfStartDate));
                } catch (Exception var31) {
                    StartDate1 = null;
                }
                //Lấy thông tin END DATE at index j
                try {
                    EndDate1 = new Date((String)((List)lsRows.get(j)).get(indexOfEndDate));
                } catch (Exception var29) {
                    EndDate1 = null;
                }
                //Lấy thông tin FILLER START at index k
                try {
                    StartFill2 = new Date((String)((List)lsRows.get(k)).get(fillstart));
                } catch (Exception var26) {
                    StartFill2 = null;
                }
                //Lấy thông tin FILLER END at index k
                try {
                    EndFill2 = new Date((String)((List)lsRows.get(k)).get(fillend));
                } catch (Exception var24) {
                    EndFill2 = null;
                }

                //Nếu assetnum của filler khác null, khác empty
                //assetnum của filler giống với assetnum
                //
                if (checkfill2 != null && checkfill2 != "" && check11.equals(checkfill2) && StartDate1.compareTo(EndFill2) <= 0 && StartFill2.compareTo(EndDate1) <= 0) {
                    this.clientSession.addWarning(new MXApplicationException("importdowntime", "importError", new String[]{"Asset and Filler: Start Date - End Date trùng khoảng thời gian của Asset dòng " + (j + 2) + " và Filler dòng " + (k + 2)}));
                }
            }

            for(k = j + 1; k < lsRows.size(); ++k) {
                try {
                    check11 = (String)((List)lsRows.get(j)).get(indexOfAssetNum);
                } catch (Exception var47) {
                    check11 = "";
                }

                try {
                    checkfill1 = (String)((List)lsRows.get(j)).get(fill);
                } catch (Exception var46) {
                    checkfill1 = "";
                }

                try {
                    check12 = (String)((List)lsRows.get(k)).get(indexOfAssetNum);
                } catch (Exception var45) {
                    check12 = "";
                }

                try {
                    checkfill2 = (String)((List)lsRows.get(k)).get(fill);
                } catch (Exception var44) {
                    checkfill2 = "";
                }

                try {
                    StartDate1 = new Date((String)((List)lsRows.get(j)).get(indexOfStartDate));
                } catch (Exception var43) {
                    StartDate1 = null;
                }

                try {
                    StartFill1 = new Date((String)((List)lsRows.get(j)).get(fillstart));
                } catch (Exception var42) {
                    StartFill1 = null;
                }

                try {
                    EndDate1 = new Date((String)((List)lsRows.get(j)).get(indexOfEndDate));
                } catch (Exception var41) {
                    EndDate1 = null;
                }

                try {
                    EndFill1 = new Date((String)((List)lsRows.get(j)).get(fillend));
                } catch (Exception var40) {
                    EndFill1 = null;
                }

                try {
                    StartDate2 = new Date((String)((List)lsRows.get(k)).get(indexOfStartDate));
                } catch (Exception var39) {
                    StartDate2 = null;
                }

                try {
                    StartFill2 = new Date((String)((List)lsRows.get(k)).get(fillstart));
                } catch (Exception var38) {
                    StartFill2 = null;
                }

                try {
                    EndDate2 = new Date((String)((List)lsRows.get(k)).get(indexOfEndDate));
                } catch (Exception var37) {
                    EndDate2 = null;
                }

                try {
                    EndFill2 = new Date((String)((List)lsRows.get(k)).get(fillend));
                } catch (Exception var36) {
                    EndFill2 = null;
                }
                //Trùng khoảng thời gian của asset
                if (check12 != null && check12 != "" && check11.equals(check12) && StartDate1.compareTo(EndDate2) <= 0 && StartDate2.compareTo(EndDate1) <= 0) {
                    this.clientSession.addWarning(new MXApplicationException("importdowntime", "importError", new String[]{"Asset: Start Date - End Date trùng khoảng thời gian của Asset dòng " + (j + 2) + " và dòng " + (k + 2)}));
                }

                //Trùng khoảng thời gian của filler
                if (checkfill1 != null && checkfill1 != "" && checkfill2 != null && checkfill2 != "" && checkfill1.equals(checkfill2) && StartFill1.compareTo(EndFill2) <= 0 && StartFill2.compareTo(EndFill1) <= 0) {
                    this.clientSession.addWarning(new MXApplicationException("importdowntime", "importError", new String[]{"Filler: Start Date - End Date trùng khoảng thời gian của Filler dòng " + (j + 2) + " và dòng " + (k + 2)}));
                }
            }
        }

    }

    private static String removeUTF8BOM(String s) {
        if (s.startsWith("\ufeff")) {
            s = s.substring(1);
        }

        return s;
    }

    public void validateRequiredFields(String startDate, String endDate, String filler, String fillerStart, String fillerEnd, String C, String I, String L, String T, int row) {
        if (startDate.equals("") || endDate.equals("")) {
            this.clientSession.addWarning(new MXApplicationException("importdowntime", "importError", new String[]{"Asset Bắt buộc nhập Start Date và End Date ở dòng " + row}));
        }

        if (!filler.equals("") && (fillerStart.equals("") || fillerEnd.equals(""))) {
            this.clientSession.addWarning(new MXApplicationException("importdowntime", "importError", new String[]{"Filler Bắt buộc nhập Start Date và End Date ở dòng " + row}));
        }

        if (C.equals("") || I.equals("") || L.equals("") || T.equals("")) {
            this.clientSession.addWarning(new MXApplicationException("importdowntime", "importError", new String[]{"Bắt buộc nhập C, I, L, T ở dòng " + row}));
        }

    }
}
