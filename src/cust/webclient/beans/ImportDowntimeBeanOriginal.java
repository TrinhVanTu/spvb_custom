package cust.webclient.beans;

import psdi.security.ConnectionKey;
import psdi.server.MXServer;
import psdi.util.MXApplicationException;
import psdi.util.MXException;
import psdi.webclient.system.beans.DataBean;
import psdi.webclient.system.controller.UploadFile;
import psdi.webclient.system.controller.WebClientEvent;

import java.io.*;
import java.nio.ByteBuffer;
import java.nio.charset.CharacterCodingException;
import java.nio.charset.Charset;
import java.rmi.RemoteException;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;

public class ImportDowntimeBeanOriginal extends DataBean {

    public static final String UTF8_BOM = "\uFEFF";

    protected void initialize() throws MXException, RemoteException {

    }

    public int execute() throws MXException, RemoteException {
        if (this.clientSession.runLongOp(this, "execute")) {
            WebClientEvent currentEvent = this.clientSession.getCurrentEvent();
            UploadFile file = (UploadFile) this.app.get("importfile");
            try {
                //execute
                //validate
                if(!file.getFileName().contains(".csv")){
                    this.clientSession.showMessageBox(currentEvent, "importdowntime", "importfailed", new String[]{"File .csv is required"});
                }

                HashMap<String, List<List<String>>> hashMap = readFlatFile(file);

                List<String> listHeaders = hashMap.get("header").get(0);
                List<List<String>> listValues = hashMap.get("rows");
                int numRows = listValues.size();

                validateNumberOfRows(numRows);

                //date range overlap

                validateDateRangesOverlapRecordsImport(listHeaders, listValues);

                //check dupplication
                validateDuplicationRecords(listHeaders, listValues);

                if(listValues.size() == 0){
                    this.clientSession.showMessageBox(currentEvent, "importdowntime", "importfailed", new String[]{"Import Failed. Please check the file content again. The file is in wrong format"});
                }

                for (int i = 0; i < listValues.size(); i++) {
                    HashMap<String, String> hmap = pustIntoHmap(listHeaders, listValues.get(i));

                    validateRequiredFields(hmap.get("START DATE"), hmap.get("END DATE"), hmap.get("FILLER"), hmap.get("FILLER START"), hmap.get("FILLER END"), hmap.get("C"), hmap.get("I"), hmap.get("L"), hmap.get("T"), i+2);

                    if (!hmap.get("START DATE").isEmpty() && !hmap.get("END DATE").isEmpty()) {

                        /*validateTimeExceedCurrent(hmap.get("START DATE"), i + 2);
                        validateTimeExceedCurrent(hmap.get("END DATE"), i + 2);*/

                        isThisDateValid(hmap.get("START DATE"), "mm/dd/yyyy HH:mm", i + 2);
                        isThisDateValid(hmap.get("END DATE"), "mm/dd/yyyy HH:mm", i + 2);
                        validateEnddateAfterStartDate(new Date(hmap.get("START DATE")), new Date(hmap.get("END DATE")), i + 2);
                    }

                    if (!hmap.get("FILLER START").isEmpty() && !hmap.get("FILLER END").isEmpty()) {

                        /*validateTimeExceedCurrent(hmap.get("START DATE"), i + 2);
                        validateTimeExceedCurrent(hmap.get("END DATE"), i + 2);
                        validateTimeExceedCurrent(hmap.get("FILLER START"), i + 2);
                        validateTimeExceedCurrent(hmap.get("FILLER END"), i + 2);*/

                        isThisDateValid(hmap.get("FILLER START"), "mm/dd/yyyy HH:mm", i + 2);
                        isThisDateValid(hmap.get("FILLER END"), "mm/dd/yyyy HH:mm", i + 2);
                        validateEnddateAfterStartDate(new Date(hmap.get("FILLER START")), new Date(hmap.get("FILLER END")), i + 2);
                    }

                    //get value siteid from hmap

                    //hmap.get("SITEID")
                    validateDateRangesOverlap(hmap.get("ASSETNUM"), hmap.get("SITEID"), hmap.get("START DATE"), hmap.get("END DATE"), i + 2);

                    if(!hmap.get("FILLER").equals("")){
                        validateDateRangesOverlap(hmap.get("FILLER"), hmap.get("SITEID"), hmap.get("FILLER START"), hmap.get("FILLER END"), i + 2);
                    }
                    validateAsset(hmap.get("ASSETNUM"), hmap.get("SITEID"), i + 2);
                    if(!hmap.get("FILLER").equals("")){
                        validateAsset(hmap.get("FILLER"), hmap.get("SITEID"),i + 2);
                    }

                    validateDowntimeCode(hmap.get("DOWNTIME CODE"), i + 2);
                }

                if (!this.clientSession.hasWarnings()) {
                    int checkComplete = 0;
                    for (int i = 0; i < listValues.size(); i++) {
                        HashMap<String, String> hmap = pustIntoHmap(listHeaders, listValues.get(i));
                        insertIntoASSETSTATUS(hmap);
                        checkComplete++;
                    }
                    if (checkComplete == listValues.size()) {
                        //this.clientSession.showMessageBox(new MXAccessException("importdowntime", "importAllSuccess"));
                        if(checkComplete != 0)
                        this.clientSession.showMessageBox(currentEvent, "importdowntime", "importsuccess", new String[]{"Import Successful"});
                        this.fireDataChangedEvent();
                    }
                }

            } catch (Exception var8) {
                this.clientSession.showMessageBox(currentEvent, "importdowntime", "importfailed", new String[]{"Import Failed"});
                return 1;
            }
        }
        return 1;
    }

    //pust value to hmap
    public HashMap<String, String> pustIntoHmap(List<String> header, List<String> values) {
        HashMap<String, String> hmap = new HashMap<String, String>();
        hmap.clear();
        int size = header.size();
        for (int i = 0; i < size; i++) {
            try {
                hmap.put(header.get(i), values.get(i));
            } catch (Exception e) {
                hmap.put(header.get(i), "");
            }
        }
        return hmap;
    }

    //get curent date
    private static String getFormatDateTime(String string) {
        java.util.Date date = new java.util.Date(string);
        SimpleDateFormat formatter = new SimpleDateFormat("yyyy/MM/dd HH:mm:ss");
        String strDate= formatter.format(date);
        return strDate;
    }

    //insert
    public int insertIntoASSETSTATUS(HashMap<String, String> hmap) throws MXException, RemoteException, SQLException {
        int id = 0;
        MXServer mxServer = MXServer.getMXServer();
        ConnectionKey conKey = mxServer.getSystemUserInfo().getConnectionKey();
        Connection con = mxServer.getDBManager().getConnection(conKey);
        try {
            PreparedStatement stmnt = null;
            // Case 1
            if (hmap.get("FILLER") == "") {

                String query2 = "INSERT INTO ASSETSTATUS(ASSETNUM, ISRUNNING, CHANGEDATE, CHANGEBY, DOWNTIME, CODE, OPERATIONAL ,LOCATION ,SITEID ,ORGID, ASSETSTATUSID, SPVB_C, SPVB_I, SPVB_L, SPVB_T, SPVB_RELATEDDOWNTIME, REMARKS, SPVB_ISSUE, SPVB_CA, SPVB_PA) VALUES (?, ?, TO_DATE(?, 'yyyy/mm/dd hh24:mi:ss'), ? ,?, ? ,? ,? ,? ,?, ASSETSTATUSSEQ.NEXTVAL, ?, ?, ?, ?, ?, ?, ?, ?, ?)";
                stmnt = con.prepareStatement(query2);
                String[] arr = getAssetInformation(hmap.get("ASSETNUM"));
                //set the parameter
                stmnt.setString(1, hmap.get("ASSETNUM"));//assetnum
                stmnt.setInt(2, 0);//isrunning
                stmnt.setString(3, getFormatDateTime(hmap.get("START DATE")));//changedate
                stmnt.setString(4, getCurrentPersonId());//changeby
                stmnt.setFloat(5, 0.0f);//downtime
                stmnt.setString(6, hmap.get("DOWNTIME CODE"));//code
                stmnt.setInt(7, 1);//operation
                stmnt.setString(8, arr[0]);//location
                stmnt.setString(9, arr[1]);//siteid
                stmnt.setString(10, arr[2]);//orgid
                //ASSETSTATUSID
                //SPVB_C
                stmnt.setString(11, hmap.get("C"));
                //SPVB_I
                stmnt.setString(12, hmap.get("I"));
                //SPVB_L
                stmnt.setString(13, hmap.get("L"));
                //SPVB_T
                stmnt.setString(14, hmap.get("T"));
                //SPVB_RELATEDDOWNTIME
                stmnt.setString(15, "0");
                //REMARKS
                stmnt.setString(16, hmap.get("REMARKS"));
                //SPVB_ISSUE
                stmnt.setString(17, hmap.get("ISSUE"));
                //SPVB_CA
                stmnt.setString(18, hmap.get("CORRECTIVE ACTION"));
                //SPVB_PA
                stmnt.setString(19, hmap.get("PREVENTIVE ACTION"));
                stmnt.executeUpdate();

                String query1 = "INSERT INTO ASSETSTATUS(ASSETNUM, ISRUNNING, CHANGEDATE, CHANGEBY, DOWNTIME, CODE, OPERATIONAL ,LOCATION ,SITEID ,ORGID, ASSETSTATUSID, SPVB_C, SPVB_I, SPVB_L, SPVB_T, SPVB_RELATEDDOWNTIME, REMARKS, SPVB_ISSUE, SPVB_CA, SPVB_PA) VALUES (?, ?, TO_DATE(?, 'yyyy/mm/dd hh24:mi:ss'), ? ,?, ? ,? ,? ,? ,?, ASSETSTATUSSEQ.NEXTVAL, ?, ?, ?, ?, ?, ?, ?, ?, ?)";
                stmnt = con.prepareStatement(query1);
                //set the parameter
                stmnt.setString(1, hmap.get("ASSETNUM"));//assetnum
                stmnt.setInt(2, 1);//isrunning
                stmnt.setString(3, getFormatDateTime(hmap.get("END DATE")));//changedate
                stmnt.setString(4, getCurrentPersonId());//changeby
                float diff = getDiffTwoTime(new Date(hmap.get("START DATE")), new Date(hmap.get("END DATE")));
                stmnt.setFloat(5, (float) diff);//downtime
                stmnt.setString(6, hmap.get("DOWNTIME CODE"));//code
                stmnt.setInt(7, 1);//operation
                stmnt.setString(8, arr[0]);//location
                stmnt.setString(9, arr[1]);//siteid
                stmnt.setString(10, arr[2]);//orgid
                //ASSETSTATUSID
                //SPVB_C
                stmnt.setString(11, hmap.get("C"));
                //SPVB_I
                stmnt.setString(12, hmap.get("I"));
                //SPVB_L
                stmnt.setString(13, hmap.get("L"));
                //SPVB_T
                stmnt.setString(14, hmap.get("T"));
                //SPVB_RELATEDDOWNTIME
                stmnt.setString(15, "");
                //REMARKS
                stmnt.setString(16, "");
                //SPVB_ISSUE
                stmnt.setString(17, hmap.get("ISSUE"));
                //SPVB_CA
                stmnt.setString(18, hmap.get("CORRECTIVE ACTION"));
                //SPVB_PA
                stmnt.setString(19, hmap.get("PREVENTIVE ACTION"));
                stmnt.executeUpdate();
            }
            //Case 2
            if (hmap.get("FILLER") != "") {

                String sqlIdentifier = "select ASSETSTATUSSEQ.NEXTVAL from dual";
                PreparedStatement pst = con.prepareStatement(sqlIdentifier);
                long myId = 0;
                synchronized (this) {
                    ResultSet rs = pst.executeQuery();
                    if (rs.next()) {
                        myId = rs.getLong(1);
                    }
                }
                String query4 = "INSERT INTO ASSETSTATUS(ASSETNUM, ISRUNNING, CHANGEDATE, CHANGEBY, DOWNTIME, CODE, OPERATIONAL ,LOCATION ,SITEID ,ORGID, ASSETSTATUSID, SPVB_C, SPVB_I, SPVB_L, SPVB_T, SPVB_RELATEDDOWNTIME, REMARKS, SPVB_ISSUE, SPVB_CA, SPVB_PA) VALUES (?, ?, TO_DATE(?, 'yyyy/mm/dd hh24:mi:ss'), ? ,?, ? ,? ,? ,? ,?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)";
                stmnt = con.prepareStatement(query4);
                String[] arr3 = getAssetInformation(hmap.get("ASSETNUM"));
                //set the parameter
                stmnt.setString(1, hmap.get("ASSETNUM"));//assetnum
                stmnt.setInt(2, 0);//isrunning
                stmnt.setString(3, getFormatDateTime(hmap.get("START DATE")));//changedate
                stmnt.setString(4, getCurrentPersonId());//changeby
                stmnt.setFloat(5, 0.0f);//downtime
                stmnt.setString(6, hmap.get("DOWNTIME CODE"));//code
                stmnt.setInt(7, 1);//operation
                stmnt.setString(8, arr3[0]);//location
                stmnt.setString(9, arr3[1]);//siteid
                stmnt.setString(10, arr3[2]);//orgid
                //ASSETSTATUSID
                stmnt.setLong(11, myId);
                //SPVB_C
                stmnt.setString(12, hmap.get("C"));
                //SPVB_I
                stmnt.setString(13, hmap.get("I"));
                //SPVB_L
                stmnt.setString(14, hmap.get("L"));
                //SPVB_T
                stmnt.setString(15, hmap.get("T"));
                //SPVB_RELATEDDOWNTIME
                stmnt.setString(16, "");
                //REMARKS
                stmnt.setString(17, hmap.get("REMARKS"));
                //SPVB_ISSUE
                stmnt.setString(18, hmap.get("ISSUE"));
                //SPVB_CA
                stmnt.setString(19, hmap.get("CORRECTIVE ACTION"));
                //SPVB_PA
                stmnt.setString(20, hmap.get("PREVENTIVE ACTION"));
                stmnt.executeUpdate();

                String query3 = "INSERT INTO ASSETSTATUS(ASSETNUM, ISRUNNING, CHANGEDATE, CHANGEBY, DOWNTIME, CODE, OPERATIONAL ,LOCATION ,SITEID ,ORGID, ASSETSTATUSID, SPVB_C, SPVB_I, SPVB_L, SPVB_T, SPVB_RELATEDDOWNTIME, REMARKS, SPVB_ISSUE, SPVB_CA, SPVB_PA) VALUES (?, ?, TO_DATE(?, 'yyyy/mm/dd hh24:mi:ss'), ? ,?, ? ,? ,? ,? ,?, ASSETSTATUSSEQ.NEXTVAL, ?, ?, ?, ?, ?, ?, ?, ?, ?)";
                stmnt = con.prepareStatement(query3);
                //set the parameter
                stmnt.setString(1, hmap.get("ASSETNUM"));//assetnum
                stmnt.setInt(2, 1);//isrunning
                stmnt.setString(3, getFormatDateTime(hmap.get("END DATE")));//changedate
                stmnt.setString(4, getCurrentPersonId());//changeby
                float diff3 = getDiffTwoTime(new Date(hmap.get("START DATE")), new Date(hmap.get("END DATE")));
                stmnt.setFloat(5, (float) diff3);//downtime
                stmnt.setString(6, hmap.get("DOWNTIME CODE"));//code
                stmnt.setInt(7, 1);//operation
                stmnt.setString(8, arr3[0]);//location
                stmnt.setString(9, arr3[1]);//siteid
                stmnt.setString(10, arr3[2]);//orgid
                //ASSETSTATUSID
                //SPVB_C
                stmnt.setString(11, hmap.get("C"));
                //SPVB_I
                stmnt.setString(12, hmap.get("I"));
                //SPVB_L
                stmnt.setString(13, hmap.get("L"));
                //SPVB_T
                stmnt.setString(14, hmap.get("T"));
                //SPVB_RELATEDDOWNTIME
                stmnt.setString(15, "");
                //REMARKS
                stmnt.setString(16, "");
                //SPVB_ISSUE
                stmnt.setString(17, hmap.get("ISSUE"));
                //SPVB_CA
                stmnt.setString(18, hmap.get("CORRECTIVE ACTION"));
                //SPVB_PA
                stmnt.setString(19, hmap.get("PREVENTIVE ACTION"));
                stmnt.executeUpdate();

                String query2 = "INSERT INTO ASSETSTATUS(ASSETNUM, ISRUNNING, CHANGEDATE, CHANGEBY, DOWNTIME, CODE, OPERATIONAL ,LOCATION ,SITEID ,ORGID, ASSETSTATUSID, SPVB_C, SPVB_I, SPVB_L, SPVB_T, SPVB_RELATEDDOWNTIME, REMARKS, SPVB_ISSUE, SPVB_CA, SPVB_PA) VALUES (?, ?, TO_DATE(?, 'yyyy/mm/dd hh24:mi:ss'), ? ,?, ? ,? ,? ,? ,?, ASSETSTATUSSEQ.NEXTVAL, ?, ?, ?, ?, ?, ?, ?, ?, ?)";
                stmnt = con.prepareStatement(query2);
                String[] arr = getAssetInformation(hmap.get("FILLER"));
                //set the parameter
                stmnt.setString(1, hmap.get("FILLER"));//assetnum
                stmnt.setInt(2, 0);//isrunning
                stmnt.setString(3, getFormatDateTime(hmap.get("FILLER START")));//changedate
                stmnt.setString(4, getCurrentPersonId());//changeby
                stmnt.setFloat(5, 0.0f);//downtime
                stmnt.setString(6, hmap.get("DOWNTIME CODE"));//code
                stmnt.setInt(7, 1);//operation
                stmnt.setString(8, arr[0]);//location
                stmnt.setString(9, arr[1]);//siteid
                stmnt.setString(10, arr[2]);//orgid
                //ASSETSTATUSID
                //SPVB_C
                stmnt.setString(11, hmap.get("C"));
                //SPVB_I
                stmnt.setString(12, hmap.get("I"));
                //SPVB_L
                stmnt.setString(13, hmap.get("L"));
                //SPVB_T
                stmnt.setString(14, hmap.get("T"));
                //SPVB_RELATEDDOWNTIME
                stmnt.setLong(15, myId);
                //REMARKS
                stmnt.setString(16, hmap.get("REMARKS"));
                //SPVB_ISSUE
                stmnt.setString(17, hmap.get("ISSUE"));
                //SPVB_CA
                stmnt.setString(18, hmap.get("CORRECTIVE ACTION"));
                //SPVB_PA
                stmnt.setString(19, hmap.get("PREVENTIVE ACTION"));
                stmnt.executeUpdate();

                String query1 = "INSERT INTO ASSETSTATUS(ASSETNUM, ISRUNNING, CHANGEDATE, CHANGEBY, DOWNTIME, CODE, OPERATIONAL ,LOCATION ,SITEID ,ORGID, ASSETSTATUSID, SPVB_C, SPVB_I, SPVB_L, SPVB_T, SPVB_RELATEDDOWNTIME, REMARKS, SPVB_ISSUE, SPVB_CA, SPVB_PA) VALUES (?, ?, TO_DATE(?, 'yyyy/mm/dd hh24:mi:ss'), ? ,?, ? ,? ,? ,? ,?, ASSETSTATUSSEQ.NEXTVAL, ?, ?, ?, ?, ?, ?, ?, ?, ?)";
                stmnt = con.prepareStatement(query1);
                //set the parameter
                stmnt.setString(1, hmap.get("FILLER"));//assetnum
                stmnt.setInt(2, 1);//isrunning
                stmnt.setString(3, getFormatDateTime(hmap.get("FILLER END")));//changedate
                stmnt.setString(4, getCurrentPersonId());//changeby
                float diff = getDiffTwoTime(new Date(hmap.get("FILLER START")), new Date(hmap.get("FILLER END")));
                stmnt.setFloat(5, (float) diff);//downtime
                stmnt.setString(6, hmap.get("DOWNTIME CODE"));//code
                stmnt.setInt(7, 1);//operation
                stmnt.setString(8, arr[0]);//location
                stmnt.setString(9, arr[1]);//siteid
                stmnt.setString(10, arr[2]);//orgid
                //ASSETSTATUSID
                //SPVB_C
                stmnt.setString(11, hmap.get("C"));
                //SPVB_I
                stmnt.setString(12, hmap.get("I"));
                //SPVB_L
                stmnt.setString(13, hmap.get("L"));
                //SPVB_T
                stmnt.setString(14, hmap.get("T"));
                //SPVB_RELATEDDOWNTIME
                stmnt.setString(15, "");
                //REMARKS
                stmnt.setString(16, "");
                //SPVB_ISSUE
                stmnt.setString(17, hmap.get("ISSUE"));
                //SPVB_CA
                stmnt.setString(18, hmap.get("CORRECTIVE ACTION"));
                //SPVB_PA
                stmnt.setString(19, hmap.get("PREVENTIVE ACTION"));
                stmnt.executeUpdate();
            }
        } catch (SQLException var11) {
            con.rollback();
            throw new MXApplicationException("SQLException caught", var11.getMessage());
        } finally {
            if (con != null) {
                mxServer.getDBManager().freeConnection(conKey);
            }

        }

        return 1;
    }

    //read flat file

    public HashMap<String, List<List<String>>> readFlatFile(UploadFile fileupload) throws MXException, IOException {
        HashMap<String, List<List<String>>> hashMap = new HashMap<String, List<List<String>>>();
        List<String> listHeaders = new ArrayList<String>();
        List<List<String>> listValues = new ArrayList<List<String>>();
        String line = "";
        String cvsSplitBy = "\\t";
        BufferedReader br = null;
        ByteArrayOutputStream fileOutputStream = fileupload.getFileOutputStream();
        byte[] bytesarr = fileOutputStream.toByteArray();
        if (!isValidUTF8(bytesarr)) {
            this.clientSession.addWarning(new MXApplicationException("importdowntime", "importError", new String[]{"Error occurred while validating UTF-8 encoding"}));
        }
        Reader reader = new InputStreamReader(new ByteArrayInputStream(bytesarr), "UTF-16");
        try {
            br = new BufferedReader(reader);
            int i = 0;
            while ((line = br.readLine()) != null) {
                i++;
                // use comma as separator
                line = removeUTF8BOM(line);
                String[] arrStr = line.split(cvsSplitBy);
                if (i == 1) {
                    for (String str : arrStr) {
                        listHeaders.add(str != null && !str.trim().isEmpty() ? str : "");
                    }
                } else {
                    List<String> rowValues = new ArrayList<String>();
                    for (String str : arrStr) {
                        rowValues.add(str != null && !str.trim().isEmpty() ? str : "");
                    }
                    listValues.add(rowValues);
                }
            }
            List<List<String>> list = new ArrayList<List<String>>();
            list.add(listHeaders);
            hashMap.put("header", list);
            hashMap.put("rows", listValues);
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            if (br != null) {
                try {
                    br.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }
        return hashMap;
    }

    //validate asset code

    public int validateAsset(String assetnum, String siteid, int row) throws MXException, RemoteException, SQLException {
        MXServer mxServer = MXServer.getMXServer();
        ConnectionKey conKey = mxServer.getSystemUserInfo().getConnectionKey();
        Connection con = mxServer.getDBManager().getConnection(conKey);
        try {
            PreparedStatement stmnt = null;

            try {
                String query = "SELECT ASSETNUM FROM ASSET WHERE SITEID = '"+ siteid +"' AND ASSETNUM = '" + assetnum + "'";

                stmnt = con.prepareStatement(query);
            } catch (SQLException var10) {
                System.out.println(var10);
            }

            if (stmnt.execute()) {
                ResultSet results = stmnt.getResultSet();
                if (results.next() == false) {
                    //throw new MXApplicationException("importdowntime", "assetnuminvalid");
                    this.clientSession.addWarning(new MXApplicationException("importdowntime", "importError", new String[]{"Error occurred while validating asset " + assetnum + " at row: " + row}));
                }
            }
        } catch (SQLException var11) {
            throw new MXApplicationException("SQLException caught", var11.getMessage());
        } finally {
            if (con != null) {
                mxServer.getDBManager().freeConnection(conKey);
            }

        }

        return 1;
    }

    //validate downtime code

    public int validateDowntimeCode(String downtimeCode, int row) throws MXException, RemoteException, SQLException {
        MXServer mxServer = MXServer.getMXServer();
        ConnectionKey conKey = mxServer.getSystemUserInfo().getConnectionKey();
        Connection con = mxServer.getDBManager().getConnection(conKey);
        try {
            PreparedStatement stmnt = null;

            try {
                String query = "SELECT VALUE FROM ALNDOMAIN WHERE DOMAINID = 'DOWNCODE' AND VALUE = '" + downtimeCode + "'";

                stmnt = con.prepareStatement(query);
            } catch (SQLException var10) {
                System.out.println(var10);
            }

            if (stmnt.execute()) {
                ResultSet results = stmnt.getResultSet();
                if (results.next() == false) {
                    //throw new MXApplicationException("importdowntime", "downtimecodeinvalid");
                    this.clientSession.addWarning(new MXApplicationException("importdowntime", "importError", new String[]{"Error occurred while validating downtimecode " + downtimeCode + " at row: " + row}));
                }
            }
        } catch (SQLException var11) {
            throw new MXApplicationException("SQLException caught", var11.getMessage());
        } finally {
            if (con != null) {
                mxServer.getDBManager().freeConnection(conKey);
            }

        }

        return 1;
    }

    //validate Date and Time
    public boolean isThisDateValid(String dateToValidate, String dateFromat, int row) {

        if (dateToValidate == null) {
            return false;
        }

        SimpleDateFormat sdf = new SimpleDateFormat(dateFromat);
        sdf.setLenient(false);

        try {

            //if not valid, it will throw ParseException
            Date date = sdf.parse(dateToValidate);
            System.out.println(date);

        } catch (ParseException e) {
            //dd-MMM-yy HH:mm:ss mandatory
            this.clientSession.addWarning(new MXApplicationException("importdowntime", "importError", new String[]{"Error occurred while validating date " + dateToValidate + " at row: " + row}));
            return false;
        }

        return true;
    }

    //validate size of file
    //validate max rows
    public void validateNumberOfRows(int num) {
        if (num > 200) {
            this.clientSession.addWarning(new MXApplicationException("importdowntime", "importError", new String[]{"Error occurred while validating number of rows exceeds 200"}));
        }
    }

    //validate start date and endate

    public boolean validateEnddateAfterStartDate(Date startDate, Date endDate, int row) {
        boolean bl = startDate.before(endDate);
        if (!bl) {
            this.clientSession.addWarning(new MXApplicationException("importdowntime", "importError", new String[]{"Error occurred while validating end date greater than start date " + "at row " + row}));
        }
        return bl;
    }

    //validate file type

    //validate duplicatiom

    //get asset information

    public String[] getAssetInformation(String assetnum) throws MXException, RemoteException, SQLException {
        String[] arr = new String[3];
        MXServer mxServer = MXServer.getMXServer();
        ConnectionKey conKey = mxServer.getSystemUserInfo().getConnectionKey();
        Connection con = mxServer.getDBManager().getConnection(conKey);
        try {
            PreparedStatement stmnt = null;

            try {
                String query = "SELECT LOCATION, SITEID, ORGID FROM ASSET WHERE ASSETNUM = '" + assetnum + "'";

                stmnt = con.prepareStatement(query);
            } catch (SQLException var10) {
                System.out.println(var10);
            }

            if (stmnt.execute()) {
                ResultSet results = stmnt.getResultSet();
                if (results.next() == false) {
                    //throw new MXApplicationException("importdowntime", "notexist");
                    this.clientSession.addWarning(new MXApplicationException("importdowntime", "importError", new String[]{"Error occurred while validating asset information: " + assetnum}));
                } else {
                    String location = results.getString(1) != null ? results.getString(1) : "";
                    String siteid = results.getString(2) != null ? results.getString(2) : "";
                    String orgid = results.getString(3) != null ? results.getString(3) : "";
                    arr[0] = location;
                    arr[1] = siteid;
                    arr[2] = orgid;
                }
            }
        } catch (SQLException var11) {
            throw new MXApplicationException("SQLException caught", var11.getMessage());
        } finally {
            if (con != null) {
                mxServer.getDBManager().freeConnection(conKey);
            }

        }

        return arr;
    }

    //validate duplication

    public void validateDuplicationRecords(List<String> headers, List<List<String>> lsRows) {
        int indexOfAssetNum = 0;
        int indexOfStartDate = 0;
        for (int i = 0; i < headers.size(); i++) {
            if (headers.get(i).equals("ASSETNUM")) {
                indexOfAssetNum = i;
            }
            if (headers.get(i).equals("START DATE")) {
                indexOfStartDate = i;
            }
        }
        for (int j = 0; j < lsRows.size(); j++) {
            for (int k = j + 1; k < lsRows.size(); k++) {
                String check11;
                String check12;
                String check21;
                String check22;
                try {
                    check11 = lsRows.get(j).get(indexOfAssetNum);

                } catch (Exception ex) {
                    check11 = "";
                }
                try {
                    check12 = lsRows.get(k).get(indexOfAssetNum);
                } catch (Exception ex) {
                    check12 = "";
                }
                try {
                    check21 = lsRows.get(j).get(indexOfStartDate);

                } catch (Exception ex) {
                    check21 = "";
                }
                try {
                    check22 = lsRows.get(k).get(indexOfStartDate);
                } catch (Exception ex) {
                    check22 = "";
                }
                if (check11.equals(check12) && check21.equals(check22)) {
                    this.clientSession.addWarning(new MXApplicationException("importdowntime", "importError", new String[]{"Error occurred while validating duplication records at row " + (j + 2) + " row " + (k + 2)}));
                }
            }

        }
    }

    //validate Date Ranges Overlap
    public void validateDateRangesOverlap(String assetnum, String siteid, String startDate, String endDate, int row) throws RemoteException, MXException {
        String[] arr = new String[3];
        MXServer mxServer = MXServer.getMXServer();
        ConnectionKey conKey = mxServer.getSystemUserInfo().getConnectionKey();
        Connection con = mxServer.getDBManager().getConnection(conKey);
        try {
            PreparedStatement stmnt = null;

            try {
                String sql = "SELECT  ASSETNUM FROM ASSETSTATUS WHERE assetnum = '" + assetnum + "' and siteid = '" + siteid + "' and changedate between TO_DATE(:1, 'MM/DD/YYYY HH24:MI') and TO_DATE(:2, 'MM/DD/YYYY HH24:MI')";
                //String sql = "SELECT  ASSETNUM FROM ASSETSTATUS WHERE assetnum = '" + assetnum + "' and siteid = '" + siteid + "' and TO_DATE(changedate) >= TO_DATE(:1, 'MM/DD/YYYY HH24:MI') AND TO_DATE(changedate) <= TO_DATE(:2, 'MM/DD/YYYY HH24:MI')";
                stmnt = con.prepareStatement(sql);
                stmnt.setString(1, startDate);
                stmnt.setString(2, endDate);
            } catch (SQLException var10) {
                System.out.println(var10);
            }

            if (stmnt.execute()) {
                ResultSet results = stmnt.getResultSet();
                if (results.next() == true) {
                    //throw new MXApplicationException("importdowntime", "notexist");
                    this.clientSession.addWarning(new MXApplicationException("importdowntime", "importError", new String[]{"Error occurred while validating Start Date and End Date must not fall within the date range of an existing downtime report for asset " + assetnum + " at row " + row}));
                }
            }
        } catch (SQLException var11) {
            throw new MXApplicationException("SQLException caught", var11.getMessage());
        } finally {
            if (con != null) {
                mxServer.getDBManager().freeConnection(conKey);
            }

        }
    }

    //get user upload

    public String getCurrentPersonId() throws RemoteException, MXException {
        return this.clientSession.getUserInfo().getUserName();
    }

    //get Diff time
    public float getDiffTwoTime(Date startDate, Date endDate) {
        float diff = endDate.getTime() - startDate.getTime();
        float diffHours = diff / (60 * 60 * 1000) % 24;
        return diffHours;
    }

    //get Orgid by asset

    //is Encoding UTF8
    public static boolean isValidUTF8(final byte[] bytes) {

        try {
            Charset.availableCharsets().get("UTF-16").newDecoder().decode(ByteBuffer.wrap(bytes));

        } catch (CharacterCodingException e) {

            return false;
        }

        return true;
    }

    public void validateDateRangesOverlapRecordsImport(List<String> headers, List<List<String>> lsRows) {
        int indexOfAssetNum = 0;
        int indexOfStartDate = 0;
        int indexOfEndDate = 0;
        for (int i = 0; i < headers.size(); i++) {
            if (headers.get(i).equals("ASSETNUM")) {
                indexOfAssetNum = i;
            }
            if (headers.get(i).equals("START DATE")) {
                indexOfStartDate = i;
            }
            if (headers.get(i).equals("END DATE")) {
                indexOfEndDate = i;
            }
        }
        for (int j = 0; j < lsRows.size(); j++) {
            for (int k = j + 1; k < lsRows.size(); k++) {
                String check11;
                String check12;
                Date StartDate1;
                Date EndDate1;
                Date StartDate2;
                Date EndDate2;
                try {
                    check11 = lsRows.get(j).get(indexOfAssetNum);

                } catch (Exception ex) {
                    check11 = "";
                }
                try {
                    check12 = lsRows.get(k).get(indexOfAssetNum);
                } catch (Exception ex) {
                    check12 = "";
                }
                try {
                    StartDate1 = new Date(lsRows.get(j).get(indexOfStartDate));//StartDate1
                } catch (Exception ex) {
                    StartDate1 = null;
                }
                try {
                    EndDate1 = new Date(lsRows.get(j).get(indexOfEndDate));//EndDate1
                } catch (Exception ex) {
                    EndDate1 = null;
                }
                try {
                    StartDate2 = new Date(lsRows.get(k).get(indexOfStartDate));//StartDate2

                } catch (Exception ex) {
                    StartDate2 = null;
                }
                try {
                    EndDate2 = new Date(lsRows.get(k).get(indexOfEndDate));//EndDate2
                } catch (Exception ex) {
                    EndDate2 = null;
                }
                if (check11.equals(check12) && (StartDate1.compareTo(EndDate2) <= 0 && StartDate2.compareTo(EndDate1) <= 0)) {
                    this.clientSession.addWarning(new MXApplicationException("importdowntime", "importError", new String[]{"Error occurred while validating two Date Ranges Overlap" + (j + 2) + " row " + (k + 2)}));
                }
            }

        }
    }

    private static String removeUTF8BOM(String s) {
        if (s.startsWith(UTF8_BOM)) {
            s = s.substring(1);
        }
        return s;
    }

    /*public void validateTimeExceedCurrent(String date, int row) {
        Date currentDate = new Date();
        if(new Date(date).compareTo(currentDate) > 0){
            this.clientSession.addWarning(new MXApplicationException("importdowntime", "importError", new String[]{"Error occurred while validating the date " + date + " is greater than current date at row " + row}));
        }
    }*/

    public void validateRequiredFields(String startDate, String endDate,String filler, String fillerStart, String fillerEnd, String C	,String I, String L, String	T, int row)
    {
        if(startDate.equals("") || endDate.equals("")){
            this.clientSession.addWarning(new MXApplicationException("importdowntime", "importError", new String[]{"Start Date and End Date is required at row " + row}));
        }
        if(!filler.equals("") && (fillerStart.equals("") || fillerEnd.equals(""))){
            this.clientSession.addWarning(new MXApplicationException("importdowntime", "importError", new String[]{"Filler Start and Filler End is required at row " + row}));
        }
        if(C.equals("") || I.equals("") || L.equals("") || T.equals("")){
            this.clientSession.addWarning(new MXApplicationException("importdowntime", "importError", new String[]{"C, I, L, T is required at row " + row}));
        }

    }
}
